<?php
// phpcs:ignoreFile

namespace Drupal\samhsa_video_widget\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'SamhsaVideoWidgetDefaultFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "SamhsaVideoWidgetDefaultFormatter",
 *   label = @Translation("SAMHSA YouTube Video"),
 *   field_types = {
 *     "samhsa_video_widget"
 *   }
 * )
 */
class SamhsaVideoWidgetDefaultFormatter extends FormatterBase {

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {

      // Render each element as markup.
      $element[$delta] = [
        '#type'                  => 'markup',
        '#markup'                => "$item->video_title",
        '#video_title'           => "$item->video_title",
        '#video_description'     => "$item->video_description",
        '#video_length'          => "$item->video_length",
        '#youtube_url'           => "$item->youtube_url",
        '#mp3_audio_title'       => "$item->mp3_audio_title",
        '#mp3_audio_description' => "$item->mp3_audio_description",
        '#mp3_audio_length'      => "$item->mp3_audio_length",
        '#mp3_audio_file'        => "$item->mp3_audio_file",
        '#theme'                 => "samhsa_video_widget",
        '#delta'                 => "$delta",
      ];

      if (isset($item->{'poster_image_' . $langcode})) {
        $element[$delta]['#poster_image'] = $item->{'poster_image_' . $langcode};
      }
      if (isset($item->{'caption_file_' . $langcode})) {
        $element[$delta]['#caption_file'] = $item->{'caption_file_' . $langcode};
      }
    }

    return $element;
  }

}
