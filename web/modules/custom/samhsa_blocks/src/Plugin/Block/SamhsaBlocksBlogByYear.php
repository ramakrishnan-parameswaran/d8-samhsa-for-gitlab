<?php

namespace Drupal\samhsa_blocks\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a 'Book navigation' block.
 *
 * @Block(
 *   id = "samhsa_blog_by_year_block",
 *   admin_label = @Translation("Blogs by year block"),
 *   category = @Translation("SAMHSA")
 * )
 */
class SamhsaBlocksBlogByYear extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $markup = '<ul class="blog">';

    $database = \Drupal::database();
    $result = $database->query(
      "SELECT date_format(str_to_date(field_publication_date_value,'%Y-%m-%d'),'%Y') as year,
        count(field_publication_date_value) as count
        FROM {node__field_publication_date}
        where entity_id in (select nid from {node_field_data} where type=:type and status=:status)
        group by year
        order by year desc", [':type' => 'article', ':status' => '1']
    );
    // Just using a static query for this case.
    $records = $result->fetchAll();

    foreach ($records as $row) {
      $year_link = '<a href = "/blog/year/' . $row->year . '">' . $row->year . '</a>';
      $markup .= '<li class="blog__category-item">' . $year_link . ' (' . $row->count . ')</li>';
    }
    $markup .= '</ul>';

    $block['#markup'] = $markup;

    return $block;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['samhsa_blocks_blogs_by_year'] = $form_state->getValue('samhsa_blocks_blogs_by_year');
  }

}
