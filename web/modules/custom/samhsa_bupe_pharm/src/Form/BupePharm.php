<?php

namespace Drupal\samhsa_bupe_pharm\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Bupe pharmacy finder form.
 */
class BupePharm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bupe_pharm_lookup';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['header'] = [
      '#type'   => 'markup',
      '#markup' => "<p>To verify a practitioner's DATA waiver, search using " . "his or her last name and DEA registration number.</p>",
    ];
    $form['practitioner'] = [
      '#type'      => 'textfield',
      '#title'     => $this->t('Practitioner Last Name'),
      '#maxlength' => 32,
      '#size'      => 32,
      '#required'  => TRUE,
    ];
    $form['dea_number'] = [
      '#type'      => 'textfield',
      '#title'     => $this->t('DEA Registration Number'),
      '#maxlength' => 32,
      '#size'      => 32,
      '#required'  => TRUE,
    ];
    $form['submit'] = [
      '#type'   => 'submit',
      '#value'  => $this->t('Submit'),
      '#weight' => '1',
    ];
    $form['footer'] = [
      '#type'   => 'markup',
      '#markup' => "<p>Each practitioner's DEA license gives two registration numbers. " . "<strong>Search using the first number</strong>, which generally starts with A, B, F or M.</p>",
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @phpcs:disable Generic.CodeAnalysis.UselessOverridingMethod.Found
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $uri = "https://buprenorphine.samhsa.gov/api/pharmacist-lookup.php?dea_registration_nmbr=";
    $uri .= $form_state->getValue('dea_number') . "&name_last=" . rawurlencode($form_state->getValue('practitioner'));

    try {
      $response = \Drupal::httpClient()
        ->get($uri, ['headers' => ['Accept' => 'text/plain']]);
      $data = (string) $response->getBody();
      if (empty($data)) {
        return FALSE;
      }
    }
    catch (RequestException $e) {
      return FALSE;
    }

    $json_response = Json::decode($data);

    if (isset($json_response['data']['attributes']['name_last'])) {

      $msg_result = $json_response['data']['attributes']['name_first'] . ' ';
      $msg_result .= $json_response['data']['attributes']['name_last'] . ' is a certified Buprenorphine Provider. ';
      \Drupal::messenger()->addMessage($msg_result);
      $msg_result = 'DEA Registration Number: ' . $json_response['data']['attributes']['dea_registration_nmbr'];
      \Drupal::messenger()->addMessage($msg_result);
      $msg_result = 'Licensed State: ' . $json_response['data']['attributes']['license_state'];
      \Drupal::messenger()->addMessage($msg_result);
      $msg_result = 'Date Certified: ' . substr($json_response['data']['attributes']['certification_date'], 0, -9);
      \Drupal::messenger()->addMessage($msg_result);

      $msg_result = 'Certified for ' . $json_response['data']['attributes']['number_patients'] . ' patients.';
      \Drupal::messenger()->addMessage($msg_result);

    }
    elseif ($json_response['errors']['status'] == "404") {

      $msg_result = $form_state->getValue('practitioner') . ' is not a Buprenorphine Certified Provider.<br />';
      $msg_result .= 'DEA Registration Number: ' . $form_state->getValue('dea_number');
      \Drupal::messenger()->addMessage(t($msg_result), 'error');

    }
    else {
      $msg_result = 'The following information is not available at this time. Please try again later.';
      \Drupal::messenger()->addMessage($msg_result, 'error');
    }
  }

}
