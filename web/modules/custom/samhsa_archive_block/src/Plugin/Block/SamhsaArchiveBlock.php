<?php

namespace Drupal\samhsa_archive_block\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a block with drop-down that launches Month-Year filtered content.
 *
 * @Block(
 *   id = "samhsa_archive_block",
 *   admin_label = @Translation("SAMHSA Archive block"),
 * )
 */
class SamhsaArchiveBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $database = \Drupal::database();
    $query = $database->select('node_field_data', 'nfd');
    $query->condition('type', 'article');
    $query->condition('status', 1);
    $query->addExpression("FROM_UNIXTIME(created, '%m')", 'month');
    $query->addExpression("FROM_UNIXTIME(created, '%Y')", 'year');
    $query->groupBy('year');
    $query->groupBy('month');
    $query->orderBy('year', 'DESC');
    $query->orderBy('month', 'DESC');
    $query->addExpression('COUNT(created)', 'count');

    $result = $query->execute();
    $records = $result->fetchAll();

    // $currentPath = $_SERVER['PHP_SELF'];
    // $pathInfo = pathinfo($currentPath);
    // $hostName = $_SERVER['HTTP_HOST'];
    // $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';
    // $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';
    $stub_url = \Drupal::request()->getSchemeAndHttpHost() . "/";
    // $stub_url = $host . $pathInfo['dirname']."/";
    $markup = '<div class="js-form-item form-item js-form-type-select form-type-select js-form-item- form-item-">';
    $markup .= '<select class="form-select js-samhsaArchiveJump" aria-label="Archive Month Search" required>';
    $markup .= '<option class="fake_o_placeholder" value="" disabled selected hidden>-- Select Month --</option>';
    foreach ($records as $r) {
      $month_name = format_date(mktime(0, 0, 0, $r->month, 1), 'custom', 'F');
      $markup .= '<option value="' . $stub_url . 'blog/archive/' . $r->year . '/' . $r->month .
              '">' . $month_name . html_entity_decode('&nbsp;') . $r->year .
              html_entity_decode('&nbsp;') . '(' . $r->count . ')</option>';
    }
    $markup .= '</select></div>';

    $select_box['#markup'] = $markup;
    $select_box['#allowed_tags'] = ['div', 'select', 'option'];
    // This is necessary for picking up new content immediately.
    $select_box['#cache']['max-age'] = 0;
    $select_box['#attached'] = [
    // No JS behaviors at all without this.
      'library' => ['samhsa_archive_block/samhsa-archive-block'],
    ];
    return $select_box;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['archive_block_settings'] = $form_state->getValue('archive_block_settings');
  }

}
