(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.samhsaArchiveJump = {
    attach: function (context, settings) {
      $('.js-samhsaArchiveJump').on('change', function () {
    	  window.location = $(this).find(':selected').val();
      });
    }
  };
}(jQuery, Drupal, drupalSettings));
