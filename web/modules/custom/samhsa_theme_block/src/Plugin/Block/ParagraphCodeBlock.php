<?php

namespace Drupal\samhsa_theme_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides the Paragraph page block.
 *
 * @Block(
 *   id = "paragraph_code",
 *   admin_label = @Translation("JS and CSS Code for Parargraph Types"),
 *   category = "SAMHSA Theme Blocks",
 * )
 */
class ParagraphCodeBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'paragraph_code',
      '#title' => 'Paragraph Code',
    ];
  }

}
