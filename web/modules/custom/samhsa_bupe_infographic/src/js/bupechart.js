(function (Drupal, drupalSettings) {
	Drupal.behaviors.bupeChart = {
    attach: function (context, settings) {
	  var colors 	= ["#334b56", "#8a180e", "#C47B12"];
      var num_30 	= drupalSettings.samhsa_bupe_infographic.BupeChart.inpNum1;
      var num_100 	= drupalSettings.samhsa_bupe_infographic.BupeChart.inpNum2;
      var num_275 	= drupalSettings.samhsa_bupe_infographic.BupeChart.inpNum3;
      var num_tl 	= num_30 + num_100 + num_275;
      var TITLE_STRING = " Total: " + num_tl;
      var ID_NAME = drupalSettings.samhsa_bupe_infographic.BupeChart.domObj;

   // Load the Visualization API and the piechart package.
  	google.charts.load('current', {
  		packages: ['corechart']
  	});
  	google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
          var data = google.visualization.arrayToDataTable([["Status", "Numbers"], ["100 Patient Certified", num_100], ["30 Patient Certified", num_30], ["275 Patient Certified", num_275]]);

          var options = { chartArea :  { left : 10, top : 20, width : "90%", height : "90%" },
                              width : "90%", height : "160", colors : colors, legend :  {
                          alignment : "end", position : "labeled", textStyle :  { italic : "true", fontSize : 15 } },
                       pieSliceText : "value", pieSliceTextStyle :  { color : "#ebddba", fontSize : 13, bold : "true" },
                              title : TITLE_STRING, pieHole : 0.1
                        }; // end options
       
                        // Create a formatter: This example uses object literal notation to define the options.
                        var formatter = new google.visualization.NumberFormat( { pattern : "#,###" });
                        formatter.format(data, 1); // Reformat our data.

                        var chart = new google.visualization.PieChart(document.getElementById(ID_NAME));
                        chart.draw(data, options);    
     } // end drawChart
    } // end attached function
  } // end Behavior
})(Drupal, drupalSettings);