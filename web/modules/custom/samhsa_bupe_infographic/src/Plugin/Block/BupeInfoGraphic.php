<?php
// phpcs:ignoreFile

namespace Drupal\samhsa_bupe_infographic\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Component\Serialization\Json;

/**
 * Provides a 'BupeInfoGraphic' block.
 *
 * @Block(
 *  id = "bupe_info_graphic",
 *  admin_label = @Translation("Bupe info graphic"),
 * )
 */
class BupeInfoGraphic extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup'      => $this->_bupe_infographic_make(),
      '#cache'       => ['max-age' => 0],
    ];
  }

  /**
   *
   */
  private function _bupe_infographic_make() {

    $uri = "https://buprenorphine.samhsa.gov/api/current-physician-counts.php";
    try {
      $response = \Drupal::httpClient()->get($uri, ['headers' => ['Accept' => 'text/plain']]);
      $data = (string) $response->getBody();
      if (empty($data)) {
        return FALSE;
      }
    }
    catch (RequestException $e) {
      return FALSE;
    }
    $json_response = Json::decode($data);

    $cert30         = (int) $json_response['certified_30_total'];
    $cert30_past_yr = (int) $json_response['certified_30_past_year'];
    $cert30_past_90 = (int) $json_response['certified_30_past_90'];
    $cert30_past_60 = (int) $json_response['certified_30_past_60'];
    $cert30_past_30 = (int) $json_response['certified_30_past_30'];

    $cert100         = (int) $json_response['certified_100_total'];
    $cert100_past_yr = (int) ($json_response['certified_100_past_year']);
    $cert100_past_90 = (int) ($json_response['certified_100_past_90']);
    $cert100_past_60 = (int) ($json_response['certified_100_past_60']);
    $cert100_past_30 = (int) ($json_response['certified_100_past_30']);

    $cert275         = (int) ($json_response['certified_275_total']);
    $cert275_past_yr = (int) ($json_response['certified_275_past_year']);
    $cert275_past_90 = (int) ($json_response['certified_275_past_90']);
    $cert275_past_60 = (int) ($json_response['certified_275_past_60']);
    $cert275_past_30 = (int) ($json_response['certified_275_past_30']);

    $cert_all = $cert30 + $cert100 + $cert275;
    $cert30_perc  = round(100 * $cert30 / $cert_all);
    $cert100_perc = round(100 * $cert100 / $cert_all);
    $cert275_perc = round(100 * $cert275 / $cert_all);

    $cert_all_past_yr     = $cert30_past_yr + $cert100_past_yr + $cert275_past_yr;
    $cert30_perc_past_yr  = round(100 * $cert30_past_yr / $cert_all_past_yr);
    $cert100_perc_past_yr = round(100 * $cert100_past_yr / $cert_all_past_yr);
    $cert275_perc_past_yr = round(100 * $cert275_past_yr / $cert_all_past_yr);

    $cert_all_past_90     = $cert30_past_90 + $cert100_past_90 + $cert275_past_90;
    $cert30_perc_past_90  = round(100 * $cert30_past_90 / $cert_all_past_90);
    $cert100_perc_past_90 = round(100 * $cert100_past_90 / $cert_all_past_90);
    $cert275_perc_past_90 = round(100 * $cert275_past_90 / $cert_all_past_90);

    $cert_all_past_60     = $cert30_past_60 + $cert100_past_60 + $cert275_past_60;
    $cert30_perc_past_60  = round(100 * $cert30_past_60 / $cert_all_past_60);
    $cert100_perc_past_60 = round(100 * $cert100_past_60 / $cert_all_past_60);
    $cert275_perc_past_60 = round(100 * $cert275_past_60 / $cert_all_past_60);

    $cert_all_past_30     = $cert30_past_30 + $cert100_past_30 + $cert275_past_30;
    $cert30_perc_past_30  = round(100 * $cert30_past_30 / $cert_all_past_30);
    $cert100_perc_past_30 = round(100 * $cert100_past_30 / $cert_all_past_30);
    $cert275_perc_past_30 = round(100 * $cert275_past_30 / $cert_all_past_30);

    $block_string  = '<p id="bupe_info_graphic">&nbsp;</p>';
    $block_string .= '<p><p>Learn how SAMHSA evaluates the buprenorphine waiver program under the Drug Addiction ' .
                        'Treatment Act of 2000 (DATA 2000) and tracks the number of DATA-waived practitioners.<p>';

    $block_string .= '<table><tbody>' .
        '<tr><th>Time Frame</th><th>30 Cert</th><th>%</th><th>100 Cert</th><th>%</th><th>275 Cert</th><th>%</th><th>Total</th></tr>' .
        '<tr><td>Past 30 days</td><td>' . $cert30_past_30 . '</td><td>' . $cert30_perc_past_30 . '</td><td>' . $cert100_past_30 .
            '</td><td>' . $cert100_perc_past_30 . '</td><td>' . $cert275_past_30 . '</td><td>' . $cert275_perc_past_30 . '</td><td>' . $cert_all_past_30 . '</td></tr>' .
        '<tr><td>Past 60 days</td><td>' . $cert30_past_60 . '</td><td>' . $cert30_perc_past_60 . '</td><td>' . $cert100_past_60 .
            '</td><td>' . $cert100_perc_past_60 . '</td><td>' . $cert275_past_60 . '</td><td>' . $cert275_perc_past_60 . '</td><td>' . $cert_all_past_60 . '</td></tr>' .
        '<tr><td>Past 90 days</td><td>' . $cert30_past_90 . '</td><td>' . $cert30_perc_past_90 . '</td><td>' . $cert100_past_90 .
            '</td><td>' . $cert100_perc_past_90 . '</td><td>' . $cert275_past_90 . '</td><td>' . $cert275_perc_past_90 . '</td><td>' . $cert_all_past_90 . '</td></tr>' .
        '<tr><td>Last Year</td><td>' . $cert30_past_yr . '</td><td>' . $cert30_perc_past_yr . '</td><td>' . $cert100_past_yr .
            '</td><td>' . $cert100_perc_past_yr . '</td><td>' . $cert275_past_yr . '</td><td>' . $cert275_perc_past_yr . '</td><td>' . $cert_all_past_yr . '</td></tr>' .
        '<tr><td>Current</td><td>' . $cert30 . '</td><td>' . $cert30_perc . '</td><td>' . $cert100 .
            '</td><td>' . $cert100_perc . '</td><td>' . $cert275 . '</td><td>' . $cert275_perc . '</td><td>' . $cert_all . '</td></tr>' .
        '</tbody></table>';

    $block_string .= '<p>Data Updated Daily';

    return $block_string;
  }

}
