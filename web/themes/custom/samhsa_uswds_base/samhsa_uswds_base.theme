<?php

/**
 * @file
 * Functions to support theming in the theme samhsa_uswds_base.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_alter().
 */
function samhsa_uswds_base_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // SAMHSAWCM-9064 - Disable chosen on this form, it's not pretty.
  if (preg_match('/webform_submission_speaker_request_form_node_\d*_add_form/', $form_id)) {
    $form['elements']['event_address']['#chosen'] = FALSE;
  }
}

/**
 * Prepares variables for the node.html.twig template.
 */
function samhsa_uswds_base_preprocess_node(&$variables) {
  global $base_url;
  $base_url_parts = parse_url($base_url);
  $host = $base_url_parts['host'];

  if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
    $protocol = 'https://';
  }
  else {
    $protocol = 'http://';
  }
  $variables['base_path'] = $protocol . $host;
}

/**
 * Prepares variables for the block.html.twig template.
 */
function samhsa_uswds_base_preprocess_block(&$variables) {
  $node = FALSE;
  $route_match = \Drupal::routeMatch();

  if ($route_match->getRouteName() == 'entity.node.canonical') {
    $node = $route_match->getParameter('node');
    if ($node) {
      $variables['last_date'] = $node->getChangedTime();
    }
  }
}

/**
 * Search the head element for the /core/misc/favicon and remove it.
 */
function samhsa_uswds_base_page_attachments_alter(&$page) {
  foreach ($page['#attached'] as $key => $element) {
    if ($key === 'html_head_link' && !empty($element)) {
      foreach ($element as $core_favicon => $item) {
        if (isset($item[0]['rel']) && $item[0]['rel'] === 'shortcut icon') {
          unset($page['#attached']['html_head_link'][$core_favicon]);
        }
      }
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function samhsa_uswds_base_preprocess_page_title(&$variables) {

  // SAMHSAWCM-9785 Added checks so we aren't accessing empty elements.
  $node = \Drupal::routeMatch()->getParameter('node');
  if (isset($node)) {
    $variables['hide_title'] = $node->field_hide_title->value ?? FALSE;
  }
}

/**
 * Implements hook_preprocess_page().
 *
 * @phpcs:ignore Drupal.Files.LineLength.TooLong
 */
function samhsa_uswds_base_preprocess_page(&$variables) {
  $page = 'page';
  if (!empty($variables['page']['sidebar_first']) && !empty($variables['page']['sidebar_second'])) {
    $variables['region_classes']['main'] = "main-layout with--content with--sidebar-first with--sidebar-second";
    $variables['page_classes'] = "with--header without--menus with--preface-first with--preface-second without--preface-third without--preface-fourth with--highlighted with--content without--sidebar-first without--sidebar-second with--postscript-first without--postscript-second without--postscript-third with--postscript-fourth with--footer";
  }
  else {
    if ($variables['page']['sidebar_first']) {
      $variables['region_classes']['main'] = "main-layout with--content with--sidebar-first without--sidebar-second";
      $variables['page_classes'] = "with--header without--menus with--preface-first with--preface-second without--preface-third without--preface-fourth with--highlighted with--content without--sidebar-first without--sidebar-second with--postscript-first without--postscript-second without--postscript-third with--postscript-fourth with--footer";
    }
    else {
      if ($variables['page']['sidebar_second']) {
        $variables['region_classes']['main'] = "main-layout with--content without--sidebar-first with--sidebar-second";
        $variables['page_classes'] = "with--header without--menus with--preface-first with--preface-second without--preface-third without--preface-fourth with--highlighted with--content without--sidebar-first without--sidebar-second with--postscript-first without--postscript-second without--postscript-third with--postscript-fourth with--footer";
      }
      // Unset sidebar-first for grants_awards content type
      // (with url /grants/awards/[dddd]/).
      else {
        if (preg_match("/\/grants\/awards\/\d{4}\/.+/", \Drupal::request()
          ->getRequestUri())) {
          unset($variables['page']['sidebar_first']);
          $variables['region_classes']['main'] = "main-layout with--content without--sidebar-first without--sidebar-second";
          $variables['page_classes'] = "with--header without--menus with--preface-first with--preface-second without--preface-third without--preface-fourth with--highlighted with--content without--sidebar-first without--sidebar-second with--postscript-first without--postscript-second without--postscript-third with--postscript-fourth with--footer";
        }

        else {
          if (\Drupal::service('path.matcher')->isFrontPage()) {
            $variables['#attached']['library'][] = 'samhsa_uswds_base/home_page_carousel';
          }
        }
      }
    }
  }
}

/**
 * Implements hook_preprocess_region().
 */
function samhsa_uswds_base_preprocess_region(&$vars) {
  // Add a region class in the format region--REGION-NAME.
  $altered_region_id = str_replace("_", "-", $vars['region']);
  $vars['attributes']['class'][] = 'region--' . $altered_region_id;
}

/**
 * Default theme function for all filter forms.
 */
function samhsa_uswds_base_preprocess_views_exposed_form(&$variables) {
  $form = $variables['form'] ?? NULL;

  // Check if view is NAC-Meetings-Block.
  // SAMHSAWCM-9785 Added checks so we aren't accessing empty elements.
  if (!empty($form['form']['#attributes']['data-drupal-selector']) &&
      $form['form']['#attributes']['data-drupal-selector'] == 'views-exposed-form-nac-meetings-nac-meetings-block') {
    // Remove size setting from view dropdown.
    if (isset($form['field_council_list_value']['#size'])) {
      unset($form['field_council_list_value']['#size']);
    }
  }
}

/**
 * Implements hook_preprocess_pager().
 */
function samhsa_uswds_base_preprocess_pager(&$variables) {
  // Access current pager using Pager service.
  $pagerRef = $variables['pager']['#element'];
  $pager = \Drupal::service('pager.manager')->getPager($pagerRef);

  // Get total number of pages.
  $pager_max = $pager->getTotalPages();

  // Pass total number of pages to template.
  $variables['pager_max'] = $pager_max;
}
