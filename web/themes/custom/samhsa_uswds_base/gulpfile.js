const autoprefixer = require('gulp-autoprefixer');
const browserify = require('browserify');
const buffer = require('vinyl-buffer');
const gulp = require('gulp');
const sass = require('gulp-sass');
const source = require('vinyl-source-stream');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');

gulp.task('css', function() {
  return gulp
    .src(['src/sass/style.scss'])
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        includePaths: ['node_modules/uswds/src/stylesheets'],
        outputStyle: 'compressed'
      })
    )
    .pipe(
      autoprefixer({
        // NB: browsers are read from the 'browserslist' field in
        // package.json
        cascade: false
      })
    )
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/css'));
});

// Icons and images.
//---------------------------------------------
gulp.task('dist-images', function() {
  return gulp.src('./images/dist_img/*')
    .pipe(gulp.dest('./dist/img/'));
});
gulp.task('usa-icons', function() {
  return gulp.src('./node_modules/uswds/src/img/usa-icons/**/*.svg')
    .pipe(gulp.dest('./dist/img/usa-icons/'));
});
gulp.task('usa-icons-bg', function() {
  return gulp.src('./node_modules/uswds/dist/img/usa-icons-bg/**/*.svg')
    .pipe(gulp.dest('./dist/img/usa-icons-bg/'));
});
gulp.task('social-icons', function() {
  return gulp.src('./node_modules/uswds/src/img/social-icons/**/*.svg')
    .pipe(gulp.dest('./dist/img/social-icons/'));
});
gulp.task('alerts', function() {
  return gulp.src('./node_modules/uswds/src/img/alerts/**/*.svg')
    .pipe(gulp.dest('./dist/img/alerts/'));
});

// Included fonts (that we probably aren't using but are in our CSS
//---------------------------------------------
gulp.task('roboto-mono-font', function() {
  return gulp.src('./node_modules/uswds/src/fonts/roboto-mono/*')
    .pipe(gulp.dest('./dist/fonts/roboto-mono'));
});
gulp.task('source-sans-pro-font', function() {
  return gulp.src('./node_modules/uswds/src/fonts/source-sans-pro/*')
    .pipe(gulp.dest('./dist/fonts/source-sans-pro'));
});
gulp.task('merriweather-font', function() {
  return gulp.src('./node_modules/uswds/src/fonts/merriweather/*')
    .pipe(gulp.dest('./dist/fonts/merriweather'));
});

gulp.task('uswds', function() {
  return browserify({
    entries: 'src/js/main.js',
    debug: true
  })
    .transform('babelify', {
      global: true,
      presets: ['@babel/preset-env']
    })
    .bundle()
    .pipe(source('main.js'))
    .pipe(buffer())
    .pipe(
      sourcemaps.init({
        loadMaps: true
      })
    )
    .pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/js'));
});

// Compress javascript.
gulp.task('javascript', function() {
  return gulp.src('src/js/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'));
});

// Grouped and utility tasks
//---------------------------------------------
// Standard watcher for developers working on scss files
gulp.task('watch', function() {
  gulp.watch(['src/**/*.scss'], gulp.series('css'));
  gulp.watch(['src/**/*.js'], gulp.series('javascript'));
});

// Javascript
gulp.task('js', gulp.series('javascript' , 'uswds'));

// Images/icons
gulp.task('assets', gulp.series('dist-images' , 'usa-icons', 'usa-icons-bg',
  'social-icons', 'alerts'));

// Fonts
gulp.task('fonts', gulp.series('roboto-mono-font' , 'merriweather-font',
  'source-sans-pro-font'));

// Build it all
gulp.task('build', gulp.series('css' , 'js', 'assets', 'fonts'));

// Set a default task.
gulp.task('default', gulp.series('build'));
