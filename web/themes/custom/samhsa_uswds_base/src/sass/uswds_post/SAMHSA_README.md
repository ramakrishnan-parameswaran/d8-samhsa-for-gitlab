# Purpose of this directory

This directory is included *after* USWDS core files and is intended to override styles from core.

This directory should contain no globally scoped variables. It should consist of SCSS rules intended to override USWDS core rules. SAMHSA specific rules should be made in non 'uswds' folders.

This directory overrides styles defined in:

web/themes/custom/samhsa_uswds_base/node_modules/uswds/src/stylesheets
