# Purpose of this directory

This directory is included prior to inclusion of USWDS source files.

This directory should contain *all* of the variable overrides we are doing for the USWDS theme. These files must contain
variable overrides only and no markup. Markup overrides need to go in the uswds_post folder

These files include variables files from:

web/themes/custom/samhsa_uswds_base/node_modules/uswds/src/stylesheets/theme

Files prefixed with _uswds-theme- are files copied from that directory.
