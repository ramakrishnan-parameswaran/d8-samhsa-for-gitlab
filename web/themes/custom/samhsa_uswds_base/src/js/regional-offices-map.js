(function ($, Drupal) {
    $(document).ready(function(){
        if($('body').hasClass('user-logged-in')) {
            $('map[name="sr-image-map"] area').each(function(i, j) {
                j = i + 1;
                $('#sr-id-' + j).on('mouseenter', function(e) {
                    $('.sr-tooltip-' + j).parents().siblings().children('.sr-tooltip').css({'display':'none'}); 
                    if(j >= 1 && j <= 4) {
                        $('.sr-tooltip-' + j).css({'left': e.pageX - ( $('.sr-image-map').offset().left + $('.sr-tooltip-' + j).width() ), 'top': e.pageY - $('.sr-image-map').offset().top, 'display':'block'});  
                    } else if(j >= 5 && j <= 7) {
                        $('.sr-tooltip-' + j).css({'left': e.pageX - ( $('.sr-image-map').offset().left + $('.sr-tooltip-' + j).width()/2 ), 'top': e.pageY - $('.sr-image-map').offset().top, 'display':'block'});  
                    } else {
                        $('.sr-tooltip-' + j).css({'left': e.pageX - $('.sr-image-map').offset().left, 'top': e.pageY - $('.sr-image-map').offset().top, 'display':'block'});  
                    }
                });
              });
        } else {
            $('map[name="sr-image-map"] area').each(function(i, j) {
                j = i + 1;
                $('#sr-id-' + j).on('mouseenter', function(e) {
                    $('.sr-tooltip-' + j).parents().siblings().children('.sr-tooltip').css({'display':'none'}); 
                    if(j >= 1 && j <= 4) {
                        $('.sr-tooltip-' + j).css({'left': e.pageX - $('.sr-tooltip-' + j).width(), 'top': e.pageY - 10, 'display':'block'});  
                    } else if(j >= 5 && j <= 7) {
                            $('.sr-tooltip-' + j).css({'left': e.pageX - $('.sr-tooltip-' + j).width()/2, 'top': e.pageY - 10, 'display':'block'});  
                    } else {
                        $('.sr-tooltip-' + j).css({'left': e.pageX - 10, 'top': e.pageY - 10, 'display':'block'});  
                    }
                    
                });
              });
        }

        $('.sr-image-map').on('mouseleave', function() {
            $('.sr-tooltip').css('display','none'); 
        });

        $(document).ready(function(e) {
            $('.sr-image-map').rwdImageMaps();
        });
    });
})(jQuery, Drupal);
