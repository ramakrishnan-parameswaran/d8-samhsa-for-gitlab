(function (Drupal, $, window) {
  $(document).ready(function () {
    //ADD BOLD TO DBHIS COLLECTIONS IN LEFT NAV WHEN ON DBHIS PAGE
    if (window.location.href.indexOf('/resource/dbhis/') > 0) {
      $('li.menu-item a')
        .filter('[title="New DBHIS Resource Center"]')
        .addClass("dbhis");
      $('li.menu-item a.dbhis').css("font-weight", "bold");
    }

    // Apply some 508 to Marijuana Quiz Iframe.
    if (window.location.href.indexOf('/marijuana-quiz') > 0) {
      $('iframe.h5p-iframe')
        .attr('title', 'H5P Quiz iFrame')
        .attr('alt', 'Take the quiz to test your marijuana IQ')
      ;
    }

    //REPLACE SVG.EXT ICON WITH FONT AWESOME SVG ICON
    $("a.ext")
      .append("<span class='exitDisclaimer'><a href='https://www.samhsa.gov/disclaimer' aria-label='Link to SAMHSA Exit Disclaimer'><i class='fas fa-external-link-alt' ></i></a></span>");
    $("article.node--type-resource .resource-link .field__item a.ext")
      .attr("target", "_blank");

    // Featured resources card.
    $('.ebrc-featured-resources').click(function () {
      window.location = $(this)
        .find('a')
        .attr('href');
      return false;
    });

    // Removing text links from /sitemap.
    $('div.sitemap ul.sitemap-menu ul.sitemap-menu li')
      .has('span.no-link')
      .css('display', 'none');

    // Click the "Apply" button to fix data values on MAT page.
    $(function () {
      if ($('div.block-views-blockbup-physician-count-national-table--block-1 div.view-content')
        .children().length > 1) {
        $('input[value="Apply"]').click();
      }
    });
  });

  $(window).bind("load", function () {
    // 508 - Add Aria Label to Font Awesome SVGs
    $("svg.svg-inline--fa").attr("aria-label", "Icon used for display");
  });

  // EBPRC filter accordion.
  Drupal.behaviors.filterAccordion = {
    attach: function () {
      var resourceLabel = $('fieldset[data-drupal-selector=edit-field-ebp-resource-type-target-id] legend');
      var resourcePortal = $('.views-exposed-form fieldset[data-drupal-selector=edit-field-ebp-portal-target-id]');
      var resourceContent = resourceLabel.next();
      var windowWidth = $(window).width();
      var results = $('h2.search-results');
      // For iPad & smaller devices.
      if (windowWidth <= 863) {
        $(resourceLabel).addClass('active').next().slideUp();
        $(resourceLabel).once().click(function (e) {
          e.preventDefault();
          resourcePortal.css({'min-height': 270 + 'px'});
          results.css({top: 504 + 'px'});
          var dropDown = $(this).closest('div').find(resourceContent);
          $(this)
            .closest(resourceLabel)
            .find(resourceContent)
            .not(dropDown)
            .slideDown();
          if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(resourcePortal).css({'min-height': 450 + 'px'});
            results.css({top: 685 + 'px'});
          }
          else {
            $(this)
              .closest(resourceLabel)
              .find('.active')
              .removeClass('active');
            $(this).addClass('active');
          }
          dropDown.stop(false, true).slideToggle(0);
        });
      }

      $('.views-exposed-form div[data-drupal-selector=edit-actions] input[value=Reset]')
        .on('click keypress', function (e) {
          e.stopPropagation();
          e.preventDefault();
          location.href = location.origin + location.pathname;
        });
    }
  };

  //Responsive image maps
  Drupal.behaviors.imageMaps = {
    attach: function (context, settings) {
      $('img[usemap]').once('imageMaps').rwdImageMaps();
    }
  };

  // execute mobile menu
  $(document).ready(function () {
    $(function () {
      $('#dl-menu').dlmenu({
        animationClasses: {
          classin: 'dl-animate-in-2', classout: 'dl-animate-out-2'
        }
      });
      $(".mobile-only").parent().addClass("mobile-only");
    });
    // Evidence-Based Practices Resource Center.
    //----------------------------------------------------------------------------------------------
    var ebrcPath = window.location.pathname;
    if (ebrcPath == '/resource-search/ebp') {
      $("li.menu-item--active-trail").addClass("dl-subview dl-subviewopen");
    }
// execute menu active path
    var path = window.location.pathname.split('/');
    path = path[path.length - 1];
    if (path !== undefined) {
      $("ul.sf-menu, ul.sf-menu ul")
        .find("a[href$='" + path + "']") // gets all links that match the href
        .parents('li') // gets all list items that are ancestors of the link
        .children('a') // walks down one level from all selected li's
        .addClass('active');
    }
// More Like this section responsive tabs
  });

}(Drupal, jQuery, this));

!function (e, n, a) {
  e.behaviors.omega_samhsa = {
    attach: function (e, o) {
      function i() {
        var e = n(a).width();
        e < l && n("body")
          .find(".block-mobile-hamburger-block .menu-toggle").length ? n(".main-menu")
          .addClass("collapsible") : e >= l && n(".main-menu")
          .removeClass("collapsible open animating")
      }

      function s() {
        var e = n(a).width();
        // e < l && !n("body").find(".side-menu .menu-toggle").length ? (n(".side-menu > h2").prepend('<a class="menu-toggle" href="#" rel=”nofollow”><span class="menu-icon"></span>Menu</a>'), n(".side-menu").addClass("collapsible")) : e >= l && (n("body").find(".side-menu .menu-toggle").remove(), n(".side-menu").removeClass("collapsible open animating"))
      }

      function t() {
        var e = n(a).width();
        e < l && n("body")
          .find(".site-search .search-form-wrapper").length ? n(".search-form-wrapper")
          .appendTo(".region--preface-first") : e >= l && n("body")
          .find(".region--preface-first .search-form-wrapper").length && n(".search-form-wrapper")
          .prependTo(".head-search-wrapper")
      }

      var l = 1000;
      n(".field--name-field-section-link").hover(function () {
        n(".mega-menu-item .active").removeClass("active"), n(this)
          .parent()
          .addClass("active")
      }), n(".main-menu .paragraph--type--megamenu-data:nth-child(1)")
        .addClass("active"), n("html")
        .hasClass("touchevents") && n(".main-menu .menu-item > a")
        .once()
        .click(function (e) {
          !n(this).parent().hasClass("open") && n(this)
            .parent()
            .find(".mega-menu-wrapper").length && (e.preventDefault(), n(".main-menu .menu-item")
            .removeClass("open"), n(this).parent().addClass("open"))
        }), n(".block-mobile-hamburger-block .menu-toggle")
        .once()
        .click(function (e) {
          e.preventDefault(), n(".block-mobile-hamburger-block .menu-toggle, .main-menu")
            .addClass("animating"), n(".main-menu")
            .slideToggle()
            .promise()
            .done(function () {
              n(".main-menu.collapsible")
                .removeAttr("style"), n(".block-mobile-hamburger-block .menu-toggle, .main-menu")
                .removeClass("animating")
                .toggleClass("open")
            })
        }), n("body")
        .once()
        .on("click", ".side-menu .menu-toggle", function (e) {
          e.preventDefault(), n(this)
            .parents(".collapsible")
            .addClass("animating")
            .find("> .menu")
            .slideToggle()
            .promise()
            .done(function () {
              n(this)
                .parents(".collapsible")
                .find("> .menu")
                .removeAttr("style"), n(this)
                .parents(".collapsible")
                .removeClass("animating")
                .toggleClass("open")
            })
        }), n("body")
        .once("openfootermenus")
        .on("click", "#quicklinks-toggle-button", function (e) {
          e.preventDefault(), n(".region--postscript-second")
            .addClass("animating"), n(".postscript-menu-wrapper")
            .slideToggle()
            .promise()
            .done(function () {
              n(".postscript-menu-wrapper")
                .removeAttr("style"), n(".region--postscript-second")
                .removeClass("animating")
                .toggleClass("open")
            })
        }), n("#search-toggle").once("opensearch").click(function (e) {
        e.preventDefault(), n(".search-form-wrapper")
          .slideToggle()
          .promise()
          .done(function () {
            n(".search-form-wrapper").removeAttr("style").toggleClass("open");
            if (n(".search-form-wrapper").hasClass("open")) {
              n(".topsearchinput").focus()
            }
          })
      }), n(e).find("body").once("mobileSearchPlacement").each(function () {
        i(), s(), t()
      }), n(a).on("load", function () {
      }), n(a).resize(function () {
        i(), s(), t()
      }), n(a).scroll(function () {
      })
    }
  }
}(Drupal, jQuery, this);

(function ($, Drupal) {
  "use strict";
  //  ---------------- RESOURCE CENTER ----------------
  //  -----RESOURCE PAGE RESOURCE LINK DISPLAY
  // BUG: the following selector does not exist in USWDS
  var arialabel = $('div#block-pagetitle h1 div').text()

  $("article.node--type-resource .resource-link .field__item > a")
    .attr("aria-label", "Link to " + arialabel)
    .text("View Resource");
  $("article.node--type-resource .product-page--download > a")
    .attr("aria-label", "Link to " + arialabel);
  var dltext = $("article.node--type-resource .product-page--download > a button")
    .text();
  $("article.node--type-resource .product-page--download > a button")
    .text(dltext.replace('Download', 'View Resource'));

  if ($("article.node--type-resource div.product-page--download").length) {
    $("article.node--type-resource div.resource-link").hide();
  }

  if (~window.location.href.indexOf('/blog/year')) {
    // having to call this out separately as the years are not taxo terms, but are populated from a module
    var yearurl = window.location.href.split('?');
    yearurl = yearurl[0];
    $('.breadcrumb ol li:last-child').text(' ' + yearurl.substring(yearurl.lastIndexOf('/') + 1));
  }

  if (~window.location.href.indexOf('/grants-awards-by-state/')) {
    var count = (window.location.href.match(/\//g) || []).length;
    $('li.menu-item a')
      .filter('[title="Grant Awards By State"]')
      .addClass("grants-bold");
    $('li.menu-item a.grants-bold').css("font-weight", "bold");
    if (count == 5) {
      var tmpYear = window.location.href.split('/')[5];
      var tmpState = window.location.href.split('/')[4];
      $('body.path-grants-awards-by-state .breadcrumb ol li:eq(3)').html(' ' + '<a href="/grants-awards-by-state?year=' + tmpYear + '">' + tmpYear + '</a>');
      $('body.path-grants-awards-by-state .breadcrumb ol li:eq(3)').append(' <li> ' + tmpState + '</li>');
    }
    else {
      if (count == 7) {
        var tmpYear = window.location.href.split('/')[6];
        var tmpState = window.location.href.split('/')[4];
        $('body.path-grants-awards-by-state .breadcrumb ol li:eq(3)').html(' ' + '<a href="/grants-awards-by-state?year=' + tmpYear + '">' + tmpYear + '</a>');
        $('body.path-grants-awards-by-state .breadcrumb ol li:eq(3)').append(' <li> ' + '<a href="/grants-awards-by-state/' + tmpState + '/' + tmpYear + '">' + tmpState + '</a></li>');
        // ' ' + window.location.href.split('/')[5] + ' ' + window.location.href.split('/')[7]);
        var lastcrumb = ''
        if (window.location.href.split('/')[5] == 'discretionary') {
          lastcrumb = ' Discretionary Details';
        }
        else {
          lastcrumb = ' Non-Discretionary Details';
        }
        $('body.path-grants-awards-by-state .breadcrumb ol li:eq(4)').append('<li>' + lastcrumb + '</li>');
      }
    }
  }

  //  ---------------- PARAGRAPHS ----------------
  //  -----PARAGRAPH TYPE - 1. BASIC BLOCK
  $("div.paragraph--type--header-body").each(function (index) {
    var bb_text = $(this).find("div.field--name-field-header").text();
    var bb_header_type = $(this).find("div.field--select-header-type").html();
    var bb_select_type = $(this)
      .find("div.field--select-basic-block-type")
      .html();

    //  -----BLOCK TYPE
    $(this).addClass('clearfix').addClass(bb_select_type);
    //  -----FLOAT TYPE
    if ($(this)
      .find("div.field--select-basic-block-image-type")
      .text() == 'left') {
      $(this).addClass("left");
    }
    else {
      if ($(this)
        .find("div.field--select-basic-block-image-type")
        .text() == 'right') {
        $(this).addClass("right");
      }
    }
    //  -----HEADER TYPE
    $(this)
      .find("div.field--name-field-header")
      .html("<" + bb_header_type + " class='bb-h'>" + bb_text + "</" + bb_header_type + ">");
    //  -----BUTTON TYPE
    var buttonclass = $(this).find(".field--select-button-type").text();
    $(this)
      .find(".field--name-field-button a")
      .addClass('btn')
      .addClass(buttonclass);
  });

  //  -----PARAGRAPH TYPE - 2. MULTIPLE COLUMNS
  $("div.paragraph--type--multiple-columns").each(function (index) {
    var mcnumber = $(this)
      .find("div.field--name-field-number-of-columns")
      .text()
      .replace(/[2]+/, "two")
      .replace(/[3]+/, "three")
      .replace(/[4]+/, "four")
      .replace(/[5]+/, "five")
      .replace(/[6]+/, "six");

    $(this).addClass('clearfix').addClass(mcnumber);
  });

  //  -----PARAGRAPH TYPE - ADD PID
  $('.paragraph').each(function () {
    var pid = $(this).attr("data-quickedit-entity-id");

    $(this).attr('id', pid);
  });

  //  -----PARAGRAPH TYPE - ACCORDIONS PANEL (paragraph--type--accordion)
  $('.paragraph--type--accordion').each(function () {
    // Load State
    var accordion_header = $(this).find(".accordion-header").text();
    var accordion_header_type = $(this)
      .find(".field--select-header-type")
      .text();
    var accordion_id = accordion_header.replace(/ /g, "-");
    var accordion_start = $(this)
      .find(".field--name-field-accordion-start-state")
      .text();

    //  -----HEADER TYPE
    $(this)
      .find(".accordion-header")
      .html("<" + accordion_header_type + " class='accordion-h'>" + accordion_header + "</" + accordion_header_type + ">");

    $(this).addClass(accordion_start).addClass('panel-single');
    $('.paragraph--type--accordion-group .paragraph--type--accordion')
      .each(function () {
        $(this).removeClass('panel-single').addClass('panel-group');
      });

    if ($(this).hasClass('open')) {
      $(this)
        .find('.accordion-header .accordion-h')
        .append('<i class="fas fa-minus"></i>');
    }
    else {
      if ($(this).hasClass('closed')) {
        $(this)
          .find('.accordion-header .accordion-h')
          .append('<i class="fas fa-plus"></i>');
      }
    }
    $(this).find('.accordion-header').attr('aria-controls', accordion_id);
    $(this).find('.accordion-panel').attr('id', accordion_id);
    $('.paragraph--type--accordion').addClass('clearfix');
    $('.paragraph--type--accordion-group').addClass('clearfix');
    $('.paragraph--type--accordion.open .accordion-header')
      .attr('aria-expanded', 'true');
    $('.paragraph--type--accordion.open .accordion-panel')
      .attr('aria-hidden', 'false');
    $('.paragraph--type--accordion.closed .accordion-header')
      .attr('aria-expanded', 'false');
    $('.paragraph--type--accordion.closed .accordion-panel')
      .attr('aria-hidden', 'true');

    // On Click State
    $(this).find('.accordion-header .accordion-h').click(function () {
      $(this).parents().eq(2).toggleClass('open').toggleClass('closed');

      if ($(this).parents().eq(2).hasClass('open')) {
        var state = 'open';
      }
      else {
        if ($(this).parents().eq(2).hasClass('closed')) {
          var state = 'closed';
        }
      }

      if ($(this).parents().eq(2).hasClass("panel-group")) {
        $(this)
          .parents()
          .eq(5)
          .find('.paragraph--type--accordion')
          .not(this)
          .addClass('closed')
          .removeClass('open');
        $(this)
          .parents()
          .eq(2)
          .removeClass('closed')
          .removeClass('open')
          .addClass(state);
      }

      $('.paragraph--type--accordion.open .accordion-header')
        .attr('aria-expanded', 'true');
      $('.paragraph--type--accordion.open .accordion-panel')
        .attr('aria-hidden', 'false');
      $('.paragraph--type--accordion.open .accordion-header svg')
        .addClass('fa-minus');
      $('.paragraph--type--accordion.closed .accordion-header')
        .attr('aria-expanded', 'false');
      $('.paragraph--type--accordion.closed .accordion-panel')
        .attr('aria-hidden', 'true');
      $('.paragraph--type--accordion.closed .accordion-header svg')
        .addClass('fa-plus');
    });
  });

  //  -----PARAGRAPH TYPE - ALERTS
  //  -----ALERT MESSAGES FORM (alert-message-form-wrapper)
  // Alert CT Form uses the Admin/Seven Theme, so edit the js and css for that form in
  // web/themes/omega_samhsa_main/src/js/omega-samhsa-main-behaviors.js

  //  -----ALERT MESSAGES DISPLAY (paragraph--type--alert)
  //  ALERT MESSAGES PARAGRAPH TYPE
  $("div.alert-message-wrapper").each(function (index) {
    $('div.alert-message-wrapper').addClass('notype');

    //  ADD "ALERT TYPE" CLASS
    $("div.alert-message-wrapper div.alert-type").each(function (index) {
      var html = $(this).html();

      if ($(this).length > 0) {
        $(this)
          .parents('.alert-message-wrapper')
          .removeClass('notype')
          .addClass(html);
      }
      ;
    });

    //  ADD "ALERT TYPE" CLASS
    $("div.alert-size").each(function (index) {
      if ($(this).text() === "Alert Message - Large") {
        $(this).parents('.alert-message-wrapper').addClass('large');
      }
      else {
        if ($(this).text() === "Alert Message - Small") {
          $(this).parents('.alert-message-wrapper').addClass('small');
        }
      }
      ;
    });
  });

  //  ADD "ALERT LEFT" ICON COLUMN
  $("div.alert-message-wrapper.notype div.alert-large-right")
    .before("<div class='alert-large-left'><i class='fas fa-exclamation-triangle' title='Indicates Error Message'></i></div>");
  $("div.alert-message-wrapper.error div.alert-large-right")
    .before("<div class='alert-large-left'><i class='fas fa-exclamation-triangle' title='Indicates Error Message'></i></div>");
  $("div.alert-message-wrapper.info div.alert-large-right")
    .before("<div class='alert-large-left'><i class='fas fa-info-circle' title='Indicates Info Message'></i></div>");
  $("div.alert-message-wrapper.success div.alert-large-right")
    .before("<div class='alert-large-left'><i class='fas fa-check-circle' title='Indicates Success Message'></i></div>");

  //  HOMEPGAE SPOTLIGHT HIDE/SHOW EDIT BUTTON
  $(".samhsa-spotlight .hs-button-edit").addClass("visually-hidden");
  $(".samhsa-spotlight").mouseenter(function() {
    $(".samhsa-spotlight .hs-button-edit").removeClass("visually-hidden");
    $(".samhsa-spotlight .hs-button-edit-button").removeClass("visually-hidden");
  });
  $(".samhsa-spotlight").mouseleave(function() {
    $(".samhsa-spotlight .hs-button-edit").addClass("visually-hidden");
    $(".samhsa-spotlight .hs-button-edit-button").addClass("visually-hidden");
  });

  // blog image aspect ratio
  $(".blog__body img").parent().addClass("blog_body_image");

  // remove extra asterisk
  if ($('.form-required:contains(*)').length > 0) {
    var formrequired = $('.form-required:contains(*)');
    var formasterisk = formrequired.html().replace('*', '');
    formrequired.html(formasterisk);
  }

  //Remove hyphenated word break in left nav menu
  $('.side-menu ul li a').each(function () {
    var newhyphen = $(this).text().replace(/-/g, '‑');
    $(this).text(newhyphen);
  });

  //ADD LANG ENGLISH ATTRIBUTE TO LEFT NAVIGATION IN SPANISH PAGE
  if (~window.location.href.indexOf('/linea-nacional-ayuda')) {
    $('.side-menu a').each(function () {
      if(!$(this).hasClass('is-active')) {
        $(this).attr('lang', 'en');
      }
    });
  }

  //ADD ARIA LABEL FOR ACTIVE PAGE IN LEFT NAVIGATION
  $('.side-menu a').each(function () {
    if($(this).hasClass('is-active')) {
      $(this).attr('aria-current', 'page');
    }
  });

  //ADD LANG SPANISH ATTRIBUTE TO LEFT NAVIGATION
  if (~window.location.href.indexOf('/find-help/national-helpline')) {
    $('.side-menu a').each(function (i, e) {
      if($(e).attr('href') == '/linea-nacional-ayuda') {
        $(this).attr('lang', 'es');
      }
    });
  }

})(jQuery, Drupal);
