(function ($, Drupal) {
  "use strict";
  $('.blog__menu-toggle').click(function () {
    $('.hide-mobile').not(this).toggle('').toggleClass('mobile-open');
    $('.region--sidebar-first').toggleClass('region--box-shadow');
    $(this).toggleClass('blog__categories-open');
    if( $(this).find('a').attr('aria-expanded') == 'false' ) { $(this).find('a').attr('aria-expanded', 'true') } else { $(this).find('a').attr('aria-expanded', 'false') }
  });
})(jQuery, Drupal);
