window.Modernizr = function (e, u, i) {
  function t(e) {
    p.cssText = e;
  }

  function a(e, t) {
    return typeof e === t;
  }

  function c(e, t) {
    for (var n in e) {
      var r = e[n];
      if (!~("" + r).indexOf("-") && p[r] !== i) return "pfx" != t || r;
    }

    return !1;
  }

  function r(e, t, n) {
    var r = e.charAt(0).toUpperCase() + e.slice(1),
        o = (e + " " + v.join(r + " ") + r).split(" ");
    return a(t, "string") || void 0 === t ? c(o, t) : function (e, t, n) {
      for (var r in e) {
        var o = t[e[r]];
        if (o !== i) return !1 === n ? e[r] : a(o, "function") ? o.bind(n || t) : o;
      }

      return !1;
    }(o = (e + " " + y.join(r + " ") + r).split(" "), t, n);
  }

  function n(e, t, n, r) {
    var o,
        i,
        a,
        c = u.createElement("div"),
        l = u.body,
        s = l || u.createElement("body");
    if (parseInt(n, 10)) for (; n--;) (i = u.createElement("div")).id = r ? r[n] : d + (n + 1), c.appendChild(i);
    return o = ["&#173;", '<style id="s', d, '">', e, "</style>"].join(""), c.id = d, (l ? c : s).innerHTML += o, s.appendChild(c), l || (s.style.background = "", s.style.overflow = "hidden", a = f.style.overflow, f.style.overflow = "hidden", f.appendChild(s)), e = t(c, e), l ? c.parentNode.removeChild(c) : (s.parentNode.removeChild(s), f.style.overflow = a), !!e;
  }

  var o,
      l,
      s = {},
      f = u.documentElement,
      d = "modernizr",
      p = u.createElement(d).style,
      h = " -webkit- -moz- -o- -ms- ".split(" "),
      m = "Webkit Moz O ms",
      v = m.split(" "),
      y = m.toLowerCase().split(" "),
      g = {},
      b = [],
      E = b.slice,
      j = {}.hasOwnProperty,
      C = void 0 !== j && void 0 !== j.call ? function (e, t) {
    return j.call(e, t);
  } : function (e, t) {
    return t in e && void 0 === e.constructor.prototype[t];
  };

  for (l in Function.prototype.bind || (Function.prototype.bind = function (n) {
    var r = this;
    if ("function" != typeof r) throw new TypeError();

    var o = E.call(arguments, 1),
        i = function () {
      if (this instanceof i) {
        var e = function () {};

        e.prototype = r.prototype;
        var t = new e(),
            e = r.apply(t, o.concat(E.call(arguments)));
        return Object(e) === e ? e : t;
      }

      return r.apply(n, o.concat(E.call(arguments)));
    };

    return i;
  }), g.touch = function () {
    var t;
    return "ontouchstart" in e || e.DocumentTouch && u instanceof DocumentTouch ? t = !0 : n(["@media (", h.join("touch-enabled),("), d, ")", "{#modernizr{top:9px;position:absolute}}"].join(""), function (e) {
      t = 9 === e.offsetTop;
    }), t;
  }, g.cssanimations = function () {
    return r("animationName");
  }, g.csstransitions = function () {
    return r("transition");
  }, g) C(g, l) && (o = l.toLowerCase(), s[o] = g[l](), b.push((s[o] ? "" : "no-") + o));

  return s.addTest = function (e, t) {
    if ("object" == typeof e) for (var n in e) C(e, n) && s.addTest(n, e[n]);else {
      if (e = e.toLowerCase(), s[e] !== i) return s;
      t = "function" == typeof t ? t() : t, f.className += " " + (t ? "" : "no-") + e, s[e] = t;
    }
    return s;
  }, t(""), 0, function (e, c) {
    function l() {
      var e = h.elements;
      return "string" == typeof e ? e.split(" ") : e;
    }

    function s(e) {
      var t = p[e[i]];
      return t || (t = {}, a++, e[i] = a, p[a] = t), t;
    }

    function u(e, t, n) {
      return t = t || c, d ? t.createElement(e) : (t = (n = n || s(t)).cache[e] ? n.cache[e].cloneNode() : o.test(e) ? (n.cache[e] = n.createElem(e)).cloneNode() : n.createElem(e)).canHaveChildren && !r.test(e) ? n.frag.appendChild(t) : t;
    }

    function t(e) {
      var t,
          n,
          r,
          o,
          i,
          a = s(e = e || c);
      return !h.shivCSS || f || a.hasCSS || (a.hasCSS = (o = "article,aside,figcaption,figure,footer,header,hgroup,nav,section{display:block}mark{background:#FF0;color:#000}", i = (r = e).createElement("p"), r = r.getElementsByTagName("head")[0] || r.documentElement, i.innerHTML = "x<style>" + o + "</style>", !!r.insertBefore(i.lastChild, r.firstChild))), d || (t = e, (n = a).cache || (n.cache = {}, n.createElem = t.createElement, n.createFrag = t.createDocumentFragment, n.frag = n.createFrag()), t.createElement = function (e) {
        return h.shivMethods ? u(e, t, n) : n.createElem(e);
      }, t.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + l().join().replace(/\w+/g, function (e) {
        return n.createElem(e), n.frag.createElement(e), 'c("' + e + '")';
      }) + ");return n}")(h, n.frag)), e;
    }

    var f,
        d,
        n = e.html5 || {},
        r = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
        o = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,
        i = "_html5shiv",
        a = 0,
        p = {};
    !function () {
      try {
        var e = c.createElement("a");
        e.innerHTML = "<xyz></xyz>", f = "hidden" in e, d = 1 == e.childNodes.length || function () {
          c.createElement("a");
          var e = c.createDocumentFragment();
          return void 0 === e.cloneNode || void 0 === e.createDocumentFragment || void 0 === e.createElement;
        }();
      } catch (e) {
        d = f = !0;
      }
    }();
    var h = {
      elements: n.elements || "abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video",
      shivCSS: !1 !== n.shivCSS,
      supportsUnknownElements: d,
      shivMethods: !1 !== n.shivMethods,
      type: "default",
      shivDocument: t,
      createElement: u,
      createDocumentFragment: function (e, t) {
        if (e = e || c, d) return e.createDocumentFragment();

        for (var n = (t = t || s(e)).frag.cloneNode(), r = 0, o = l(), i = o.length; r < i; r++) n.createElement(o[r]);

        return n;
      }
    };
    e.html5 = h, t(c);
  }(this, u), s._version = "2.6.2", s._prefixes = h, s._domPrefixes = y, s._cssomPrefixes = v, s.testProp = function (e) {
    return c([e]);
  }, s.testAllProps = r, s.testStyles = n, s.prefixed = function (e, t, n) {
    return t ? r(e, t, n) : r(e, "pfx");
  }, f.className = f.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + (" js " + b.join(" ")), s;
}(this, this.document), function (e, p) {
  function f(e) {
    return "[object Function]" == o.call(e);
  }

  function h(e) {
    return "string" == typeof e;
  }

  function d() {}

  function m(e) {
    return !e || "loaded" == e || "complete" == e || "uninitialized" == e;
  }

  function v() {
    var e = b.shift();
    E = 1, e ? e.t ? y(function () {
      ("c" == e.t ? N.injectCss : N.injectJs)(e.s, 0, e.a, e.x, e.e, 1);
    }, 0) : (e(), v()) : E = 0;
  }

  function t(e, t, n, r, o) {
    return E = 0, t = t || "j", h(e) ? (a = "c" == t ? x : w, c = e, l = t, t = this.i++, n = n, r = r, o = (o = o) || N.errorTimeout, s = p.createElement(a), f = u = 0, d = {
      t: l,
      s: c,
      e: n,
      a: r,
      x: o
    }, 1 === S[c] && (f = 1, S[c] = []), "object" == a ? s.data = c : (s.src = c, s.type = a), s.width = s.height = "0", s.onerror = s.onload = s.onreadystatechange = function () {
      i.call(this, f);
    }, b.splice(t, 0, d), "img" != a && (f || 2 === S[c] ? (C.insertBefore(s, j ? null : g), y(i, o)) : S[c].push(s))) : (b.splice(this.i++, 0, e), 1 == b.length && v()), this;

    function i(e) {
      if (!u && m(s.readyState) && (d.r = u = 1, E || v(), s.onload = s.onreadystatechange = null, e)) for (var t in "img" != a && y(function () {
        C.removeChild(s);
      }, 50), S[c]) S[c].hasOwnProperty(t) && S[c][t].onload();
    }

    var a, c, l, s, u, f, d;
  }

  function c() {
    var e = N;
    return e.loader = {
      load: t,
      i: 0
    }, e;
  }

  var n,
      r = p.documentElement,
      y = e.setTimeout,
      g = p.getElementsByTagName("script")[0],
      o = {}.toString,
      b = [],
      E = 0,
      i = ("MozAppearance" in r.style),
      j = i && !!p.createRange().compareNode,
      C = j ? r : g.parentNode,
      r = e.opera && "[object Opera]" == o.call(e.opera),
      r = !!p.attachEvent && !r,
      w = i ? "object" : r ? "script" : "img",
      x = r ? "script" : w,
      a = Array.isArray || function (e) {
    return "[object Array]" == o.call(e);
  },
      l = [],
      S = {},
      s = {
    timeout: function (e, t) {
      return t.length && (e.timeout = t[0]), e;
    }
  },
      N = function (e) {
    function u(e, t, n, r, o) {
      var i = function (e) {
        for (var t, n, e = e.split("!"), r = l.length, o = e.pop(), i = e.length, o = {
          url: o,
          origUrl: o,
          prefixes: e
        }, a = 0; a < i; a++) n = e[a].split("="), (t = s[n.shift()]) && (o = t(o, n));

        for (a = 0; a < r; a++) o = l[a](o);

        return o;
      }(e),
          a = i.autoCallback;

      i.url.split(".").pop().split("?").shift(), i.bypass || (t = t && (f(t) ? t : t[e] || t[r] || t[e.split("/").pop().split("?")[0]]), i.instead ? i.instead(e, t, n, r, o) : (S[i.url] ? i.noexec = !0 : S[i.url] = 1, n.load(i.url, i.forceCSS || !i.forceJS && "css" == i.url.split(".").pop().split("?").shift() ? "c" : void 0, i.noexec, i.attrs, i.timeout), (f(t) || f(a)) && n.load(function () {
        c(), t && t(i.origUrl, o, r), a && a(i.origUrl, o, r), S[i.url] = 2;
      })));
    }

    function t(e, t) {
      function n(n, e) {
        if (n) {
          if (h(n)) e || (c = function () {
            var e = [].slice.call(arguments);
            l.apply(this, e), s();
          }), u(n, c, t, 0, i);else if (Object(n) === n) for (o in r = function () {
            var e,
                t = 0;

            for (e in n) n.hasOwnProperty(e) && t++;

            return t;
          }(), n) n.hasOwnProperty(o) && (e || --r || (f(c) ? c = function () {
            var e = [].slice.call(arguments);
            l.apply(this, e), s();
          } : c[o] = function (t) {
            return function () {
              var e = [].slice.call(arguments);
              t && t.apply(this, e), s();
            };
          }(l[o])), u(n[o], c, t, o, i));
        } else e || s();
      }

      var r,
          o,
          i = !!e.test,
          a = e.load || e.both,
          c = e.callback || d,
          l = c,
          s = e.complete || d;
      n(i ? e.yep : e.nope, !!a), a && n(a);
    }

    var n,
        r,
        o = this.yepnope.loader;
    if (h(e)) u(e, 0, o, 0);else if (a(e)) for (n = 0; n < e.length; n++) h(r = e[n]) ? u(r, 0, o, 0) : a(r) ? N(r) : Object(r) === r && t(r, o);else Object(e) === e && t(e, o);
  };

  N.addPrefix = function (e, t) {
    s[e] = t;
  }, N.addFilter = function (e) {
    l.push(e);
  }, N.errorTimeout = 1e4, null == p.readyState && p.addEventListener && (p.readyState = "loading", p.addEventListener("DOMContentLoaded", n = function () {
    p.removeEventListener("DOMContentLoaded", n, 0), p.readyState = "complete";
  }, 0)), e.yepnope = c(), e.yepnope.executeStack = v, e.yepnope.injectJs = function (e, t, n, r, o, i) {
    var a,
        c,
        l = p.createElement("script"),
        r = r || N.errorTimeout;

    for (c in l.src = e, n) l.setAttribute(c, n[c]);

    t = i ? v : t || d, l.onreadystatechange = l.onload = function () {
      !a && m(l.readyState) && (a = 1, t(), l.onload = l.onreadystatechange = null);
    }, y(function () {
      a || t(a = 1);
    }, r), o ? l.onload() : g.parentNode.insertBefore(l, g);
  }, e.yepnope.injectCss = function (e, t, n, r, o, i) {
    var a,
        t = i ? v : t || d;

    for (a in (r = p.createElement("link")).href = e, r.rel = "stylesheet", r.type = "text/css", n) r.setAttribute(a, n[a]);

    o || (g.parentNode.insertBefore(r, g), y(t, 0));
  };
}(this, document), Modernizr.load = function () {
  yepnope.apply(window, [].slice.call(arguments, 0));
};