(function ($, Drupal) {
    $(document).ready(function(){
        $(".nac-all-meetings-block-view table")
            .removeClass("tablesaw tablesaw-swipe")
            .removeAttr("id data-tablesaw-mode");
        $(".nac-all-meetings-block-view .tablesaw-bar").remove();
    });
 })(jQuery, Drupal);