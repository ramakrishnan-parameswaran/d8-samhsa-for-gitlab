(function ($, Drupal) {
  Drupal.behaviors.myModuleBehavior = {
    attach: function (context, settings) {
      $('.dl-trigger').click(function() {
        var ebrcPath = window.location.pathname;
        if (ebrcPath == '/resource-search/ebp'){
          $("li.menu-item--active-trail").addClass("dl-subviewopen cool");
          setTimeout(function() {
            $('li.dl-subviewopen').removeClass("dl-subview");
          }, 500);
        }
      })
    }
  };
})(jQuery, Drupal);
