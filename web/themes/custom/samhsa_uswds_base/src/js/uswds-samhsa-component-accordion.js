/*
* USWDS - COMPONENTS - ACCORDION
*/
// Accordion expandAll/collapseAll functionality
(function($) {
  $(".usa-accordion-expandall").click(function() {
    event.preventDefault();
    $(".usa-accordion__content").removeAttr('hidden');
    $(".usa-accordion__button").attr('aria-expanded', true);
  });
  $(".usa-accordion-collapse-all").click(function() {
    event.preventDefault();
    $(".usa-accordion__content").attr('hidden', true);
    $(".usa-accordion__button").attr('aria-expanded', false)
  });
})(jQuery);
