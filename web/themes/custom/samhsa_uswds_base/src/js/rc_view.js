
(function ($, Drupal) {
  "use strict";

  var resetElementID = "rc-reset-search-filters";

  function removeFacetParameter(url, param) {
    var urlArray = url.split("?");
    var newParameter = "";
    var updatedURL = urlArray[0];
    var currentParameter = urlArray[1];
    var temp = "";
    if (currentParameter) {
      var paramArray = currentParameter.split("&");
      for (var i = 0; i < paramArray.length; i++) {
        if (paramArray[i].split('=')[0].substring(0, 5) != param) {
          newParameter += temp + paramArray[i];
          temp = "&";
        }
      }
    }
    if (newParameter) {
      updatedURL += "?" + newParameter;
    }
    return updatedURL;
  }

  $(document).ready(function () {
    var hideList = $('.rc--hide-block');
    var filtertitle = $('.block-mobile-rc-header-h1').hide();
    $('.rc--mobile-collapsible-filters').click(function (e) {
      e.preventDefault();
      $(this).toggleClass("expanded");
      $(hideList).toggle();
      $(filtertitle).toggle();
    });

    // filter reset button
    if ($("#" + resetElementID).length) {

      var newURL = removeFacetParameter(window.location.href, "rc%5B");

      if (window.location.href != newURL) {
        $("#" + resetElementID).show();
        $("#" + resetElementID).on("click", function () {
          window.location = newURL;
        });
      } else {
        $("#" + resetElementID).hide();
      }
    }
  });
  // filter reset part deux
  $(document).ajaxStop(function () {
    if ($("#" + resetElementID).length) {

      var newURL = removeFacetParameter(window.location.href, "rc%5B");

      if (window.location.href != newURL) {
        $("#" + resetElementID).show();
        $("#" + resetElementID).on("click", function () {
          window.location = newURL;
        });
      } else {
        $("#" + resetElementID).hide();
      }
    }
  });


})(jQuery, Drupal);
