/*
* USWDS - COMPONENTS - CARD
*/

(function($) {
  $(document).ready(function () {
    $('.usa-card[class*="grid-col-12"]').addClass('usa-card-one-col');
    $('.usa-card[class*="grid-col-6"]').removeClass('usa-card-one-col').addClass('usa-card-two-col');
    $('.usa-card[class*="grid-col-4"]').removeClass('usa-card-one-col').addClass('usa-card-three-col');

    $('.usa-card__container .usa-card__img a').each(function(){
      this.href = this.href.replace('internal:', '');
    });

    $('.usa-card__container .usa-card__footer a').each(function(){
      this.href = this.href.replace('internal:', '');
    });

    $('.usa-card__container .usa-card__img a[href^="http://"], .usa-card__container .usa-card__img a[href^="https://"]').not('.usa-card__container .usa-card__img a[href*="'+location.hostname+'"]').attr('target','_blank');
    $('.usa-card__container .usa-card__footer a[href^="http://"], .usa-card__container .usa-card__footer a[href^="https://"]').not('.usa-card__container .usa-card__footer a[href*="'+location.hostname+'"]').attr('target','_blank');

    // IE card image object-fit alternative solution using modernizr
    // if ( ! Modernizr.objectfit ) {
    //  $('.usa-card__img').each(function () {
    //    var $container = $(this),
    //        imgUrl = $container.find('img').prop('src');
    //    if (imgUrl) {
    //      $container
    //        .css('backgroundImage', 'url(' + imgUrl + ')')
    //        .addClass('compat-object-fit');
    //    }
    //  });
    // }

  });
})(jQuery);



