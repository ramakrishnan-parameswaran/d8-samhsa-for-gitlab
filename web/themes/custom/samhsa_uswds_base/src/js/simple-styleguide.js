(function ($, Drupal) {
  "use strict";
  $(".sg-nav-item").click(function(e) {
    //e.preventDefault();
    var href = $(this).attr('href');

    $(".sg-style-wrapper").addClass("hidden");
    $(href).removeClass("hidden").addClass(href);
  });
})(jQuery, Drupal);
