!function (o, i) {
  "use strict";

  o("nav#block-mega ul li a.is-active").parent().addClass("is-active");
  var e = i.Modernizr,
      s = o("body");
  o.DLMenu = function (n, e) {
    this.$el = o(e), this._init(n);
  }, o.DLMenu.defaults = {
    animationClasses: {
      classin: "dl-animate-in-1",
      classout: "dl-animate-out-1"
    },
    onLevelClick: function (n, e) {
      return !1;
    },
    onLinkClick: function (n, e) {
      return !1;
    },
    backLabel: "Back",
    useActiveItemAsBackLabel: !1,
    useActiveItemAsLink: !1,
    resetOnClose: !0
  }, o.DLMenu.prototype = {
    _init: function (n) {
      this.options = o.extend(!0, {}, o.DLMenu.defaults, n), this._config();
      this.animEndEventName = {
        WebkitAnimation: "webkitAnimationEnd",
        OAnimation: "oAnimationEnd",
        msAnimation: "MSAnimationEnd",
        animation: "animationend"
      }[e.prefixed("animation")] + ".dlmenu", this.transEndEventName = {
        WebkitTransition: "webkitTransitionEnd",
        MozTransition: "transitionend",
        OTransition: "oTransitionEnd",
        msTransition: "MSTransitionEnd",
        transition: "transitionend"
      }[e.prefixed("transition")] + ".dlmenu", this.supportAnimations = e.cssanimations, this.supportTransitions = e.csstransitions, this._initEvents();
    },
    _config: function () {
      this.open = !1, this.$trigger = this.$el.children(".dl-trigger"), this.$menu = this.$el.children("ul.dl-menu"), this.$menuitems = this.$menu.find("li:not(.dl-back)"), this.$el.find("ul.dl-menu").prepend('<li class="dl-back"><a href="#" tabindex="0">&nbsp;</a><a href="#" class="dl-close-link" tabindex="0">CLOSE</a></li>'), this.$el.find("ul.dl-submenu").prepend('<li class="dl-back"><a href="#"class="dl-back-link" tabindex="0">BACK</a><a href="#" class="dl-close-link" tabindex="0">CLOSE</a></li>'), this.$back = this.$menu.find("li a.dl-back-link");
    },
    _initEvents: function () {
      var l = this;
      this.$trigger.on("click.dlmenu", function () {
        return l.open ? l._closeMenu() : (l._openMenu(), o("a.dl-close-link").on("click.dlmenu", function () {
          l._closeMenu();
        }), s.off("click").children().on("click.dlmenu", function () {
          l._closeMenu();
        })), !1;
      }), this.$menuitems.on("click.dlmenu", function (n) {
        n.stopPropagation();
        var e = o(this),
            i = e.children("ul.dl-submenu");

        if (o(n.target).is(".expand")) {
          if (0 < i.length && !o(n.currentTarget).hasClass("dl-subviewopen")) {
            var s = i.clone().css("opacity", 0).insertAfter(l.$menu),
                t = function () {
              l.$menu.off(l.animEndEventName).removeClass(l.options.animationClasses.classout).addClass("dl-subview"), e.addClass("dl-subviewopen").parents(".dl-subviewopen:first").removeClass("dl-subviewopen").addClass("dl-subview"), s.remove();
            };

            return setTimeout(function () {
              s.addClass(l.options.animationClasses.classin), l.$menu.addClass(l.options.animationClasses.classout), l.supportAnimations ? l.$menu.on(l.animEndEventName, t) : t.call(), l.options.onLevelClick(e, e.children("span.expand").text());
            }), !1;
          }

          l.options.onLinkClick(e, n);
        }
      }), this.$back.on("click.dlmenu", function (n) {
        function e() {
          l.$menu.off(l.animEndEventName).removeClass(l.options.animationClasses.classin), a.remove();
        }

        var i = o(this),
            s = i.parents("ul.dl-submenu:first"),
            t = s.parent(),
            a = s.clone().insertAfter(l.$menu);
        return setTimeout(function () {
          a.addClass(l.options.animationClasses.classout), l.$menu.addClass(l.options.animationClasses.classin), l.supportAnimations ? l.$menu.on(l.animEndEventName, e) : e.call(), t.removeClass("dl-subviewopen");
          var n = i.parents(".dl-subview:first");
          n.is("li") && n.addClass("dl-subviewopen"), n.removeClass("dl-subview");
        }), !1;
      });

      if(this.$trigger.hasClass("dl-active")) {
        this.$menu.attr("aria-hidden", "false");
        this.$menu.find("li a").attr("tabindex", "0");
      } else {
        this.$menu.attr("aria-hidden", "true");
        this.$menu.find("li a").attr("tabindex", "-1");
      }

    },
    closeMenu: function () {
      this.open && this._closeMenu();
    },
    _closeMenu: function () {
      function n() {
        e.$menu.off(e.transEndEventName), e.options.resetOnClose && e._resetMenu();
      }

      var e = this;
      this.$menu.removeClass("dl-menuopen"), this.$menu.addClass("dl-menu-toggle").attr("aria-hidden", "true"), this.$menu.find("li a").attr("tabindex", "-1"), this.$trigger.removeClass("dl-active").attr("aria-expanded", "false"), this.$trigger.parent(".dl-menuwrapper").css({
        height: 0,
        top: -67,
        position: "absolute"
      }), this.$trigger.parent().siblings(".overlay-container").hide(), this.supportTransitions ? this.$menu.on(this.transEndEventName, n) : n.call(), this.open = !1;
    },
    openMenu: function () {
      this.open || this._openMenu();
    },
    _openMenu: function () {
      var n = this;
      o("a.dl-close-link").on("click.dlmenu", function () {
        n._closeMenu();
      }), s.off("click").on("click.dlmenu", function () {
        n._closeMenu();
      }), this.$trigger.parent().siblings(".overlay-container").show(), this.$trigger.parent(".dl-menuwrapper").css({
        height: "auto",
        position: "fixed",
        top: 0
      }), this.$menu.addClass("dl-menuopen dl-menu-toggle").on(this.transEndEventName, function () {
        o(this).removeClass("dl-menu-toggle");
      }), this.$trigger.addClass("dl-active").attr("aria-expanded", "true"), this.open = !0;
      this.$menu.attr("aria-hidden", "false")
      this.$menu.find("li a").attr("tabindex", "0");
      var path = window.location.pathname.substr(1);
 			var onSubPage = (path.split('/').length >= 1);
      if (o('#dl-menu').find('li.menu-item--active-trail').length && onSubPage) {
        o('#dl-menu ul.dl-menu').addClass('dl-subview');
        o('li.menu-item--active-trail').addClass('dl-subview');
        if (o('a.is-active').parent().parent().parent().hasClass('dl-menuwrapper')) {
          if (o('a.is-active').hasClass('view-all')) {
                      o('a.is-active').parent().removeClass('dl-subview').parent().parent().removeClass('dl-subview').addClass('dl-subviewopen');
          }  else   {
                      o('a.is-active').parent().removeClass('dl-subview').parent().removeClass('dl-subview').addClass('dl-subviewopen');
          }
        } else {
                      if ( !o('a.is-active').parent().parent().parent().parent().parent().hasClass('dl-subviewopen') ) { 
                        o('a.is-active').parent().removeClass('dl-subview').parent().parent().removeClass('dl-subview').addClass('dl-subviewopen');
                      } else {
                        o('a.is-active').parent().removeClass('dl-subview').parent().parent().removeClass('dl-subview');
                      }
                      if ( !o('a.view-all').parent().hasClass('menu-item--active-trail') ) { o('a.view-all').parent().removeClass('dl-subviewopen'); }
        }
      }
    }
  };
					  

  function t(n) {
    i.console && i.console.error(n);
  }

  o.fn.dlmenu = function (e) {
    var i;
    return "string" == typeof e ? (i = Array.prototype.slice.call(arguments, 1), this.each(function () {
      var n = o.data(this, "dlmenu");
      n ? o.isFunction(n[e]) && "_" !== e.charAt(0) ? n[e].apply(n, i) : t("no such method '" + e + "' for dlmenu instance") : t("cannot call methods on dlmenu prior to initialization; attempted to call method '" + e + "'");
    })) : this.each(function () {
      var n = o.data(this, "dlmenu");
      n ? n._init() : n = o.data(this, "dlmenu", new o.DLMenu(e, this));
    }), this;
  };
}(jQuery, window);