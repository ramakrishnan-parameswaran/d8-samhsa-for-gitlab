!function (o) {
  o.fn.rwdImageMaps = function () {
    var t = this;
    return o(window).resize(function () {
      t.each(function () {
        var a;
        void 0 !== o(this).attr("usemap") && (a = o(this), o("<img />").on("load", function () {
          var i = a.attr("width"),
              n = a.attr("height");
          i && n || ((t = new Image()).src = a.attr("src"), i = i || t.width, n = n || t.height);
          var h = a.width() / 100,
              s = a.height() / 100,
              t = a.attr("usemap").replace("#", ""),
              c = "coords";
          o('map[name="' + t + '"]').find("area").each(function () {
            var t = o(this);
            t.data(c) || t.data(c, t.attr(c));

            for (var a = t.data(c).split(","), r = new Array(a.length), e = 0; e < r.length; ++e) r[e] = e % 2 == 0 ? parseInt(a[e] / i * 100 * h) : parseInt(a[e] / n * 100 * s);

            t.attr(c, r.toString());
          });
        }).attr("src", a.attr("src")));
      });
    }).trigger("resize"), this;
  };
}(jQuery);