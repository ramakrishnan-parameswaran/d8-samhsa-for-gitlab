(function ($, Drupal) {
  $(document).ready(function(){
    $('.view-id-homepage_carousel_view .view-content').each(function() {
      var $this = $(this);
      if ($this.children().length > 1) {
          $this.slick({
            prevArrow : "<button class='slick-prev slick-arrow' type='button' style=''><svg aria-label='Previous'xmlns='http://www.w3.org/2000/svg' stroke-width='2' stroke='#ffffff' width='24' height='24' viewBox='0 0 24 24'><path d='M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z'/></svg></button>",
            nextArrow : "<button class='slick-next slick-arrow' type='button' style=''><svg aria-label='Next' xmlns='http://www.w3.org/2000/svg' stroke-width='2' stroke='#ffffff' width='24' height='24' viewBox='0 0 24 24'><path d='M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z'/></svg></button>",
          dots: true,
          autoplay: true,
          accessibility: true,
          infinite: true,
          speed: 1000,
          fade: true,
          cssEase: 'linear',
          autoplaySpeed: 9000,
            responsive: [
              {
                breakpoint: 864, 
                settings: {
                  autoplay: false
                }
              },
              {
                breakpoint: 480, 
                settings: {
                  autoplay: false
                }
              }
            ]
          });
      }
    });

    $('.view-id-homepage_carousel_view .view-content').each(function() {
      var $this = $(this);
      if ($this.children().length <= 1) {
        $this.slick({
          dots: false,
          arrows: false,
          accessibility: true,
          infinite: false,
          autoplay: false
        });
      }
    });
    $('.slick-list .slide_link>a')
      .mouseover(function() {
        $('.slide_link>a span.exitDisclaimer a').css('color', '#1a6986');
      })
      .mouseout(function() {
        $('.slide_link>a span.exitDisclaimer a').css('color', '#FFFFFF');
      });
  });


})(jQuery, Drupal);
