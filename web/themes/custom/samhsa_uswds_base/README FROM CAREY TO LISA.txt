Assets = USWDS stuff imported using gulp init instructions from ../themes/contrib/uswds_base/starterkits/README.md + directories copied from ../node_modules/uswds (import paths changed in associated files - probably needs to be tested with a fine tooth comb).

Src = our custom images, js, and sass. 

Template = our custom templates copied directly from OMEGA theme.

Make sure you update gulpfile.js with your local environment info. I commented where it needs to be updated.

I'm pretty sure this gulpfile.js should work. We may need co-work.

I haven't updated any of the names of Ali's styling files. I don't have the original Omega SAMHSA Subtheme for comparison.

The main SCSS for various sections need to be updated to ensure all of the src sass files are being imported and compiled. 

IMPORTANT
The paths in uswds_base_subtheme_sass_cdhAliWorkAdded.libraries.yml need to be updated.

The image directory at the theme root should remain there until we figure out why there is an images directory at the theme root as well as in /src. It is likely that images from both directories are actively used in styling and possibly even html.
